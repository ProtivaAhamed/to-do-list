package com.example.todolist;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    //Data Handleing arrayLists
    String User_name;
    TextView text;
    Name name;
    ArrayList<String> String_Data_Of_Task_Tracker = new ArrayList<>();
    private ArrayList<TaskTracker> Cards = new ArrayList<>();
    ArrayList<TaskTracker> TaskTrackerData = new ArrayList<>();
    // end of data handleing arrayList
    String day_of_month;
    String d1;
    TextView textView;
    FloatingActionButton spk;
    int c = -1;
    FirebaseDatabase firebaseDatabase;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterTaskTracker adapterTaskTracker;
    int time = 2000;
    long backpress;
    //SpotsDialog spotsDialog = new SpotsDialog(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text=findViewById(R.id.name);
        recyclerView = findViewById(R.id.recyclerView1);
        layoutManager = new LinearLayoutManager(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        textView = findViewById(R.id.date);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        User_name=getIntent().getStringExtra("UserName");
     //
        getData();
        text.setText(User_name);

        Toast.makeText(MainActivity.this,User_name,Toast.LENGTH_SHORT).show();
        Cards.clear();
        String_Data_Of_Task_Tracker.clear();
        spk = findViewById(R.id.fab1);
        spk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate(v);
            }
        });
    }




    @Override
    public void onBackPressed() {
        if (backpress + time > System.currentTimeMillis()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(MainActivity.this, "Press again to exit", Toast.LENGTH_SHORT).show();
        }
        backpress = System.currentTimeMillis();
    }

    public void setDate(View view) {
        DialogFragment datePicker = new DatePickerFragment();
        datePicker.show(getSupportFragmentManager(), "date picker");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) { // get the date using date picker
        //  d = dayOfMonth + "/" + (month + 1) + "/" + year; // this one
        d1 = dayOfMonth + "-" + (month + 1) + "-" + year;
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        day_of_month = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
        try {
            rearrangeDate(day_of_month, d1); // will rearrange the date for the cardview
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void rearrangeDate(String day_of_month, String d) throws ParseException {
        String year, weekDay, DayWithMonth;
        String[] s = day_of_month.split(",");
        DayWithMonth = s[1];
        weekDay = s[0];
        year = s[2];
        TaskTracker t = new TaskTracker(DayWithMonth, year, weekDay, d, "0",User_name);
        if (Cards.isEmpty() == true) {
            Toast.makeText(MainActivity.this, "Card Empty", Toast.LENGTH_SHORT).show();
            SaveDataTracker(t);
        } else {
            AlreadyExsists(t);
        }
    }

    private void AlreadyExsists(TaskTracker t) { // checks whether the date already exists or not
        int flag = 0;
        for (TaskTracker t1 : Cards) {
            if (t1.getDate().equals(t.getDate())) {
                flag = 1;
                break;
            }
        }
        if (flag == 0) {
            SaveDataTracker(t);
        } else {
            Toast.makeText(MainActivity.this, "Date Already Exists", Toast.LENGTH_SHORT);
        }
    }

    private void LoadData() {
        adapterTaskTracker = new AdapterTaskTracker(this, Cards);
        recyclerView.setAdapter(adapterTaskTracker);
    }

    private void addToCart() // only unique date will be added
    {
        Cards.clear();
        if (String_Data_Of_Task_Tracker.size() == 1) {
            String[] temp = String_Data_Of_Task_Tracker.get(0).split(",");
            TaskTracker taskTracker = new TaskTracker(temp[1], temp[3], temp[2], temp[0], temp[4],temp[5]);
            if(temp[5].equals(User_name)) {
                Cards.add(taskTracker);
            }
            Log.d("TaskTrackerData", String_Data_Of_Task_Tracker.get(0));
            c++;
        } else {
            for (int i = c + 1; i < String_Data_Of_Task_Tracker.size(); i++) {
                String[] temp = String_Data_Of_Task_Tracker.get(i).split(",");
                TaskTracker taskTracker = new TaskTracker(temp[1], temp[3], temp[2], temp[0], temp[4],temp[5]);
                if(temp[5].equals(User_name)) {
                    Cards.add(taskTracker);

                }
                Log.d("TaskTrackerData", String_Data_Of_Task_Tracker.get(i));
                c++;
            }
        }
        LoadData();

    }

    // FireBase Part
    private void SaveDataTracker(TaskTracker t) {     // data updating
        DatabaseReference databaseReference = firebaseDatabase.getReference("TaskTracker");
        databaseReference.child(t.getUserId()).child(t.getDate()).setValue(t);
        Toast.makeText(MainActivity.this, "Data Saved", Toast.LENGTH_LONG).show();
        getData();
    }

    public void getData() {
        String_Data_Of_Task_Tracker.clear();
        TaskTrackerData.clear();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("TaskTracker").child(User_name);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.getChildrenCount() == 0) {
                    Toast.makeText(MainActivity.this, "DataBase Empty", Toast.LENGTH_SHORT).show();
                } else {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        TaskTracker taskTracker = dataSnapshot1.getValue(TaskTracker.class);

                        String_Data_Of_Task_Tracker.add(taskTracker.getDate() + "," + taskTracker.getDayWithMonth() + "," + taskTracker.getWeekDay() + "," + taskTracker.getYear() + "," + taskTracker.getCount()+","+taskTracker.getUserId());
                        Log.d("TaskTracker2", taskTracker.getDate() + "," + taskTracker.getDayWithMonth() + "," + taskTracker.getWeekDay() + "," + taskTracker.getYear() + "," + taskTracker.getCount()+","+taskTracker.getUserId());
                    }
               //     Toast.makeText(MainActivity.this, "Data gotten Successfully", Toast.LENGTH_SHORT).show();
                    ArrayList<String> TaskTrackerS = String_Data_Of_Task_Tracker;
                    Log.d("String_racker", Integer.toString(TaskTrackerS.size()));
                    getTask(TaskTrackerS);
                    addToCart();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void getTask(final ArrayList<String> taskTrackerS) {
        final ArrayList<String> TaskS = new ArrayList<>();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Tasks");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    Toast.makeText(MainActivity.this, "DataBase Empty", Toast.LENGTH_SHORT).show();
                } else {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Tasks task = dataSnapshot1.getValue(Tasks.class);
                        TaskS.add(task.getTittle() + "," + task.getDescription() + "," + task.getTime() + "," + task.getDate() + "," + task.getKey()+","+task.getid());
                    }
              //      Toast.makeText(MainActivity.this, " Task Data gotten Successfully", Toast.LENGTH_SHORT).show();
                    Log.d("String_task", Integer.toString(TaskS.size()));
                    ProcessData(taskTrackerS, TaskS);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void ProcessData(ArrayList<String> taskTrackerS, ArrayList<String> taskS) {
        ArrayList<TaskTracker> trackers = new ArrayList<>();
        ArrayList<Tasks> task = new ArrayList<>();
        for (String str : taskTrackerS) {
            String[] temp = str.split(",");
            TaskTracker tr = new TaskTracker(temp[1], temp[3], temp[2], temp[0], temp[4],temp[5]);
            if(temp[5].equals(User_name)) {
                trackers.add(tr);
            }
        }
        Log.d("trackers", Integer.toString(trackers.size()));
        for (String str : taskS) {
            String[] temp = str.split(",");
            Tasks tr = new Tasks(temp[0], temp[1], temp[2], temp[3], temp[4],temp[5]);
            if(temp[5].equals(User_name)) {
                task.add(tr);
            }
        }
        Log.d("tasks", Integer.toString(task.size()));
        for (TaskTracker tr : trackers) {
            int k = 0;
            for (Tasks ts : task) {
                if (ts.getDate().equals(tr.getDate()) && ts.getid().equals(tr.getUserId())) {
                    k++;
                    Log.d("matched", "matched");
                }
            }
            tr.setCount(Integer.toString(k));
            updateData(tr);
        }
    }

    private void updateData(TaskTracker tr) {
        DatabaseReference databaseReference = firebaseDatabase.getReference("TaskTracker").child(tr.getUserId());
        databaseReference.child(tr.getDate()).setValue(tr);
    }


    public void logout(View view) {
        Intent intent=new Intent(MainActivity.this,LoginPage.class);
        startActivity(intent);
    }
}

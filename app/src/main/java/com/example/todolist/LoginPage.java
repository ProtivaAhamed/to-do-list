package com.example.todolist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class LoginPage extends AppCompatActivity {
    int time = 2000;
    long backpress;
    TextInputEditText textInputEditText;
    Button sign_in, sign_up;
    String UserName;
    int SignIn = 0;
    int SignUp = 0;
    String Status = "new";
    FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        textInputEditText = findViewById(R.id.En);
        firebaseDatabase = FirebaseDatabase.getInstance();
        sign_in = findViewById(R.id.SignIn);
        sign_up = findViewById(R.id.SignUp);
        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserName = textInputEditText.getText().toString();
                SignIn = 1;
                if (UserName.equals("")) {
                    Toast.makeText(LoginPage.this, "Enter a name", Toast.LENGTH_SHORT).show();
                    SignIn = 0;
                } else {
                  //  Toast.makeText(LoginPage.this, "SignIn", Toast.LENGTH_SHORT).show();
                    getData();

                }

            }
        });
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserName = textInputEditText.getText().toString();
                SignUp = 1;
                if (UserName.equals("")) {
                    Toast.makeText(LoginPage.this, "Enter a name", Toast.LENGTH_SHORT).show();
                    SignUp = 0;
                } else {
              //      Toast.makeText(LoginPage.this, "SignUp", Toast.LENGTH_SHORT).show();
                    getData();
                }
            }
        });
    }

    public void getData() {
        final ArrayList<String> data = new ArrayList<>();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Names");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.getChildrenCount() == 0) {
                    Toast.makeText(LoginPage.this, "DataBase Empty", Toast.LENGTH_SHORT).show();
                    SendData(data);
                } else {
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Name taskTracker = dataSnapshot1.getValue(Name.class);

                        data.add(taskTracker.getName() + "," + taskTracker.getNamekey());
                        Log.d("TaskTracker2", taskTracker.getName() + "," + taskTracker.getNamekey());
                    }
                    SendData(data);
                    Log.d("String_racker", Integer.toString(data.size()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public void onBackPressed() {
        if (backpress + time > System.currentTimeMillis()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(LoginPage.this, "Press again to exit", Toast.LENGTH_SHORT).show();
        }
        backpress = System.currentTimeMillis();
    }

    private void saveName(String Name) {
        DatabaseReference databaseReference1 = firebaseDatabase.getReference("Names");
        String key = databaseReference1.push().getKey();
        Name n = new Name(Name, key);
        databaseReference1.child(n.getName()).setValue(n);
    }

    private void SendData(ArrayList<String> data) {

        if (data.isEmpty()) {
            Status = "new";
            saveName(UserName);
        } else {
            for (String str : data) {
                String[] temp = str.split(",");
                if (temp[0].equals(UserName)) {
                 Status="old";
                }
            }
        }
        if(Status.equals("new") && SignUp==1)
        {
            saveName(UserName);
        }
    //    Toast.makeText(LoginPage.this,""+Status,Toast.LENGTH_SHORT).show();
         if(SignUp==1){
             SignUp=0;
             if(Status.equals("old"))
             {
                 Toast.makeText(LoginPage.this,"This Name is already Taken",Toast.LENGTH_SHORT).show();
             }
             else if(Status.equals("new"))
             {

                 Intent intent=new Intent(LoginPage.this, MainActivity.class);
                 intent.putExtra("UserName",UserName);
                 startActivity(intent);
             }
         }
         else if(SignIn==1)
         {
             SignIn=0;
             if(Status.equals("new"))
             {
                 Toast.makeText(LoginPage.this,"Name Doesn't Exists",Toast.LENGTH_SHORT).show();
             }
             else if(Status.equals("old"))
             {
                 Intent intent=new Intent(LoginPage.this, MainActivity.class);
                 intent.putExtra("UserName",UserName);
                 startActivity(intent);
             }
         }

    }
}
